from aiogram import Dispatcher, types
from aiogram.types import InputMessageContent


class Command:
    def __init__(self, dispatcher: Dispatcher):
        self.dispatcher: Dispatcher = dispatcher
        self.title: str or None = None
        """Название команды"""
        self.name: str or None = None
        """Уникальный путь команды. По нему вызываются команды"""
        self.description: str or None = None
        """Описание команды"""
        self.inline_support: bool = False
        """Поддерживается ли inline команда"""
        self.chat_trigger_list = []
        """Слова-триггеры из чата"""

    async def handler(self, message: types.Message, **kwargs):
        pass

    def inline_handler(self, query: types.InlineQuery) -> InputMessageContent:
        pass

    async def chat_message_handler(self, message: types.Message, **kwargs):
        pass

