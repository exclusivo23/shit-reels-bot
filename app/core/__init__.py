from pathlib import Path


def get_root_folder() -> Path:
    path = Path().absolute()
    allowed = ["core", "app", "tests", "config"]
    for parent in path.parents:
        if parent.name in allowed:
            return parent.parent
    if path.name in allowed:
        return path.parent
    else:
        return path


ROOT_FOLDER: Path = get_root_folder()
