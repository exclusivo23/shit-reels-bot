
TRIGGER_TEXT = "register"


def register_command(func):
    def register():
        return func
    return register
