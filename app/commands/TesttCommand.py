from aiogram import Dispatcher, types

from core.Command import Command
from core.decorator.register_command import register_command


@register_command
class TesttCommand(Command):

    def __init__(self, dispatcher: Dispatcher):
        super().__init__(dispatcher)
        self.name = "testt"
        self.description = "Сенко"
        self.chat_trigger_list = ["сенко, я дома"]

    async def handler(self, message: types.Message, **kwargs):
        return await message.answer("Сенко")

    async def chat_message_handler(self, message: types.Message, **kwargs):
        return await message.reply("Добро пожаловать домой!")

