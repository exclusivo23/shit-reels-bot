import logging

from aiogram import Bot
from aiogram.contrib.fsm_storage.redis import RedisStorage2
from aiogram.dispatcher import Dispatcher
from peewee import Database

from app.commands import COMMANDS_ROOT_FOLDER
from app.database.models import Models
from app.database import DataBase
from config.RedisConfig import RedisConfig
from config.TelegramConfig import TelegramConfig
from core.CommandRouter import CommandRouter


class TelegramBot:
    def __init__(self):
        redis_config = RedisConfig()
        self.__config = TelegramConfig()
        self.bot = Bot(token=self.__config.token)
        self.storage = RedisStorage2(
            host=redis_config.host,
            port=redis_config.port,
            db=redis_config.db,
            pool_size=redis_config.pool_size,
            prefix=redis_config.prefix
        )
        self.dispatcher = Dispatcher(bot=self.bot, storage=self.storage)
        self.router = CommandRouter(self.dispatcher, COMMANDS_ROOT_FOLDER)
        self.router.init()
        self.database_models = Models.list()
        self.database: Database = DataBase.instance

    def init(self):
        self.init_logging()
        self.init_database()

    async def on_startup(self, dispatcher: Dispatcher):
        await self.router.register_commands_names()

    def init_logging(self):
        if self.__config.debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

    def init_database(self):
        self.database.connect()
        self.database.create_tables(self.database_models)
