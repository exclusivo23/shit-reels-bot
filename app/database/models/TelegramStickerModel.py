from peewee import TextField

from app.database import BaseModel


class TelegramStickerModel(BaseModel):
    name = TextField()
    value = TextField()
